<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class FancyBoxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css
                     = [
            'css/jquery.fancybox.min.css',
        ];
    public $js
                     = [
            'js/jquery.fancybox.min.js',
        ];
    public $depends
                     = [
            'app\assets\AppAsset',
        ];
}
