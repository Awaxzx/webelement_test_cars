<?php

namespace app\models;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int|null $brand_id
 * @property int|null $model_id
 * @property int|null $mileage
 * @property int|null $price
 * @property string|null $phone
 *
 * @property CarOption[] $carOptions
 * @property CarPhoto[] $carPhotos
 * @property Brand $brand
 * @property BrandModel $model
 */
class Car extends \yii\db\ActiveRecord
{

    /**
     * @var array
     */
    public $photos     = [];
    /**
     * @var array
     */
    public $carOptions = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'model_id', 'price', 'phone', 'photos'], 'required'],
            [['brand_id', 'model_id', 'mileage', 'price'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['brand_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => BrandModel::class, 'targetAttribute' => ['model_id' => 'id']],
            [['photos'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 3],
            [['carOptions'], 'safe'],
            ['carOptions', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'brand_id'   => 'Марка',
            'model_id'   => 'Модель',
            'mileage'    => 'Пробег',
            'price'      => 'Цена',
            'phone'      => 'Телефон',
            'photos'     => 'Фотографии',
            'carOptions' => 'Дополнительное оборудование (опции)',
        ];
    }

    /**
     * Gets query for [[CarOptions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptions()
    {
        return $this->hasMany(CarOption::class, ['car_id' => 'id']);
    }


    /**
     * Gets query for [[CarPhotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarPhotos()
    {
        return $this->hasMany(CarPhoto::class, ['car_id' => 'id']);
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(BrandModel::class, ['id' => 'model_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getOptions()
    {
        return $this->hasMany(Option::class, ['id' => 'option_id'])
                    ->viaTable(CarOption::tableName(), ['car_id' => 'id']);
    }

    /**
     * Сохраняет фотографии
     */
    public function savePhotos()
    {
        foreach ($this->photos as $file)
        {
            $fileName = uniqid() . '.' . $file->extension;
            $file->saveAs('files/' . $fileName);
            $photo       = new CarPhoto();
            $photo->name = $fileName;
            $photo->link('car', $this);
        }

        return true;
    }

    /**
     * Сохраняет опции
     */
    public function saveOptions()
    {
        CarOption::deleteAll(['car_id' => $this->id]);
        if ($this->carOptions)
        {
            foreach ($this->carOptions as $option_id)
            {
                $carOption            = new CarOption();
                $carOption->option_id = $option_id;
                $carOption->link('car', $this);
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->brand->name . ' ' . $this->model->name;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->carPhotos as $photo)
        {
            $photo->delete();
        }

        return parent::beforeDelete();
    }
}
