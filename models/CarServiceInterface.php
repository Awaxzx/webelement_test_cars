<?php
/**
 * Created by PhpStorm.
 * User: Awax
 * Date: 19.02.2020
 * Time: 10:43
 */

namespace app\models;

interface CarServiceInterface
{
    /**
     * @return Car
     */
    public function getCar();

    /**
     * @param array $data
     * @param array $photos
     * @return bool
     */
    public function add(array $data, array $photos);
}