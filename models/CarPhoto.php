<?php

namespace app\models;

use app\components\MyImage;
use Imagine\Image\ImageInterface;
use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "car_photos".
 *
 * @property int $id
 * @property int|null $car_id
 * @property string|null $name
 *
 * @property Car $car
 */
class CarPhoto extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const THUMB_1_WIDTH = 146;
    /**
     *
     */
    const THUMB_1_HEIGHT = 106;
    /**
     *
     */
    const THUMB_1_DIR = '146x106';

    /**
     *
     */
    const THUMB_2_WIDTH = 720;
    /**
     *
     */
    const THUMB_2_HEIGHT = 540;
    /**
     *
     */
    const THUMB_2_DIR = '720x540';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::class, 'targetAttribute' => ['car_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'     => 'ID',
            'car_id' => 'Car ID',
            'name'   => 'Name',
        ];
    }

    /**
     * Gets query for [[Car]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }


    /**
     * @return string
     */
    public function getOriginal()
    {
        return '/files/' . $this->name;
    }

    /**
     * @return string
     */
    public function getThumbnail1()
    {
        return '/files/' . self::THUMB_1_DIR . '/' . $this->name;
    }

    /**
     * @return string
     */
    public function getThumbnail2()
    {
        return '/files/' . self::THUMB_2_DIR . '/' . $this->name;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Make thumb 1
        MyImage::thumbnail(Image::autorotate(Yii::getAlias('@webroot' . $this->getOriginal())), self::THUMB_1_WIDTH, self::THUMB_1_HEIGHT, ImageInterface::THUMBNAIL_INSET)
               ->save(Yii::getAlias('@webroot' . $this->getThumbnail1()), ['quality' => 90]);

        // Make thumb 2
        MyImage::thumbnail(Image::autorotate(Yii::getAlias('@webroot' . $this->getOriginal())), self::THUMB_2_WIDTH, self::THUMB_2_HEIGHT, ImageInterface::THUMBNAIL_INSET)
               ->save(Yii::getAlias('@webroot' . $this->getThumbnail2()), ['quality' => 90]);

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     *{@inheritdoc}
     */
    public function afterDelete()
    {
        $original = Yii::getAlias('@webroot' . $this->getOriginal());
        $thumb1   = Yii::getAlias('@webroot' . $this->getThumbnail1());
        $thumb2   = Yii::getAlias('@webroot' . $this->getThumbnail2());

        if (file_exists($original)) unlink($original);
        if (file_exists($thumb1)) unlink($thumb1);
        if (file_exists($thumb2)) unlink($thumb2);

        return parent::afterDelete();
    }

}
