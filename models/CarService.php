<?php

namespace app\models;

Class CarService implements CarServiceInterface
{
    /**
     * @var Car
     */
    private $car;

    /**
     * CarService constructor.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    /**
     * @return Car
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param array $data
     * @param array $photos
     * @return bool
     */
    public function add(array $data, array $photos)
    {
        $this->car->load($data);
        $this->car->photos = $photos;
        if ($this->car->validate())
        {
            $savedCar     = $this->car->save(false);
            $savedPhotos  = $this->car->savePhotos();
            $savedOptions = $this->car->saveOptions();

            return $savedCar && $savedPhotos && $savedOptions;
        }

        return false;
    }


}