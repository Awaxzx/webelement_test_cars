<?php foreach ($cars as $key => $car): ?>
    <div class="row <?=($key%2)?'row_bg_gray':'row_bg_white'?>">
        <div class="col-md-3 car_img_cell">
            <img src="<?= $car->carPhotos[0]->thumbnail1 ?>" alt="<?= $car->name ?>" class="img"/>
        </div>
        <div class="col-md-3 car_data_cell">
            <a href="<?= \yii\helpers\Url::to(['cars/one', 'id' => $car->id]) ?>"><?= $car->name ?></a>
        </div>
        <div class="col-md-3 car_data_cell">
            <?= \Yii::$app->formatter->asInteger($car->price) ?> руб
        </div>
        <div class="col-md-3 car_data_cell">
            <a href="#" data-id="<?=$car->id?>" class="delete">Удалить</a>
        </div>
    </div>
<?php endforeach; ?>
<div class="row">
    <?= \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
    ]); ?>
</div>