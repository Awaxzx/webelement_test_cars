<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarForm */
/* @var $form ActiveForm */
?>
<div class="cars-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'brand_id')->dropDownList($brands, ['id' => 'brand_id', 'prompt' => 'Выберите марку']) ?>
    <?= $form->field($model, 'model_id')->dropDownList($models, ['id' => 'model_id', 'prompt' => 'Для начала выберите марку']) ?>
    <?= $form->field($model, 'mileage') ?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '+7 (999) 999-99-99',])?>
    <?= $form->field($model, 'photos[]')->fileInput(['multiple' => true])->hint('Выберите несколько файлов, используя клавишу CTRL') ?>
    <?= $form->field($model, 'carOptions')->checkboxList($options, ['separator' => ' | ', 'id'=>'hiddenCheckbox']) ?>
    <?php /*foreach($)*/ ?>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- cars-form -->
