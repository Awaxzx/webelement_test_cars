<?php

\app\assets\FancyBoxAsset::register($this);

?>
<div class="container">
    <div class="row">
        <div class="col-md-8 mt-20">
            <div>
                <a data-fancybox="gallery" href="<?= $car->carPhotos[0]->original ?>" id="photo">
                    <img src="<?= $car->carPhotos[0]->thumbnail2 ?>" alt="<?= $car->name ?>" class="img"/>
                </a>
            </div>
            <div>
                <?php foreach ($car->carPhotos as $photo): ?>
                    <a href="#"><img src="<?= $photo->thumbnail1 ?>" alt="<?= $car->name ?>"
                                     data-thumb2="<?= $photo->thumbnail2 ?>" data-original="<?= $photo->original ?>"
                                     class="gallery img"/></a>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-4 mt-20">
            <h1><?= $car->name ?></h1>
            <dl>
                <?php if ($car->mileage): ?>
                    <dt><?=$car->getAttributeLabel('mileage')?>:</dt>
                    <dd><?= \Yii::$app->formatter->asInteger($car->mileage) ?> км</dd>
                <?php endif ?>
                <dt><?=$car->getAttributeLabel('price')?>:</dt>
                <dd><?= \Yii::$app->formatter->asInteger($car->price) ?> руб</dd>
                <dt><?=$car->getAttributeLabel('phone')?>:</dt>
                <dd><?= $car->phone ?></dd>
                <?php if ($car->options): ?>
                    <dt><?=$car->getAttributeLabel('carOptions')?>:</dt>
                    <dd>
                        <ul>
                            <?php foreach ($car->options as $option): ?>
                                <li><?= $option->name ?></li>
                            <?php endforeach ?>
                        </ul>
                    </dd>
                <?php endif ?>
            </dl>
        </div>
    </div>
</div>