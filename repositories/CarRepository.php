<?php
/**
 * Created by PhpStorm.
 * User: Awax
 * Date: 14.02.2020
 * Time: 20:30
 */

namespace app\repositories;

use app\models\Car;
use app\models\Brand;
use app\models\BrandModel;
use app\models\Option;
use yii\data\Pagination;

class CarRepository
{

    /**
     * @param int $items_per_page
     * @param string $route
     * @return array
     */
    public function getCarsPaginated($items_per_page = 5, $route = 'cars/list')
    {
        $car   = Car::find();
        $pages = new Pagination(['totalCount'     => $car->count(),
                                 'pageSize'       => $items_per_page,
                                 'forcePageParam' => false,
                                 'pageSizeParam'  => false,
                                 'route'          => $route]);

        $cars = $car->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        return [$cars, $pages];
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getCarById($id)
    {
        return Car::find()->with(['carOptions', 'carPhotos'])->where(['id' => $id])->one();
    }

    /**
     * @return array
     */
    public function getBrandsForDropDownList()
    {
        return Brand::find()->select('name')->orderBy(['name' => SORT_ASC])->indexBy('id')->column();
    }

    /**
     * @return array
     */
    public function getOptionsForDropDownList()
    {
        return Option::find()->select('name')->orderBy(['name' => SORT_ASC])->indexBy('id')->column();
    }

    /**
     * @param $brand_id
     * @return array
     */
    public function getBrandModelsByBrandId($brand_id)
    {
        return BrandModel::find()->select('name')->where(['brand_id' => $brand_id])->orderBy('name')->indexBy('id')->column();
    }
}