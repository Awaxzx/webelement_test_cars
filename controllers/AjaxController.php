<?php
/**
 * Created by PhpStorm.
 * User: Awax
 * Date: 15.02.2020
 * Time: 14:06
 */

namespace app\controllers;

use \Yii;
use app\repositories\CarRepository;
use yii\filters\VerbFilter;
use yii\web\Response;


Class AjaxController extends \yii\web\Controller
{
    /**
     * @var CarRepository
     */
    private $repo;

    /**
     * AjaxController constructor.
     * @param $id
     * @param $module
     * @param array $config
     * @param CarRepository $repo
     */
    public function __construct($id, $module, $config = [], CarRepository $repo)
    {
        parent::__construct($id, $module, $config);
        $this->repo = $repo;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ['verbs' => [
            'class'   => VerbFilter::class,
            'actions' => [
                'models' => ['GET'],
                'delete' => ['POST'],
            ],
        ],];
    }


    /**
     * @param $brand_id
     * @return array
     */
    public function actionModels($brand_id)
    {
        $models = $this->repo->getBrandModelsByBrandId($brand_id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $models;
    }

    /**
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        $car = $this->repo->getCarById(Yii::$app->request->post('id'));
        if (!$car)
        {
            $data['message'] = 'Объявление не найдено!';
        }
        else
        {
            if ($car->delete())
            {
                $data['message'] = 'Объявление удалено!';

                // Список объявлений
                list($cars, $pages) = $this->repo->getCarsPaginated();
                $data['list'] = $this->renderPartial('//cars/_list', ['cars' => $cars, 'pages' => $pages]);
            }
            else
            {
                $data['message'] = 'Объявление не удалено!';
            }

        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }
}