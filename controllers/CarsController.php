<?php

namespace app\controllers;

use app\repositories\CarRepository;
use \Yii;
use app\models\Car;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


class CarsController extends \yii\web\Controller
{

    /**
     * @var CarRepository
     */
    private $repo;

    /**
     * CarsController constructor.
     * @param $id
     * @param $module
     * @param array $config
     * @param CarRepository $repo
     */
    public function __construct($id, $module, $config = [], CarRepository $repo)
    {
        parent::__construct($id, $module, $config);
        $this->repo = $repo;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ['verbs' => [
            'class'   => VerbFilter::class,
            'actions' => [
                '*'       => ['GET'],
                'add'     => ['GET', 'POST'],
                'add-car' => ['GET', 'POST'],
            ],
        ],];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        list($cars, $pages) = $this->repo->getCarsPaginated();

        return $this->render('cars', ['cars' => $cars, 'pages' => $pages]);
    }


    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionOne($id)
    {
        $car = $this->repo->getCarById($id);
        if (!$car)
        {
            throw new \yii\web\NotFoundHttpException();
        }

        return $this->render('one', ['car' => $car]);
    }


    public function actionAddCar()
    {
        $service = Yii::$container->get('CarServiceInterface');
        $brands  = $this->repo->getBrandsForDropDownList();
        $options = $this->repo->getOptionsForDropDownList();
        $models  = [];

        if (Yii::$app->request->isPost)
        {
            if ($service->add(Yii::$app->request->post(), UploadedFile::getInstancesByName('Car[photos]')))
            {
                $this->redirect(['cars/one', 'id' => $service->getCar()->id]);
            }

            $models = $this->repo->getBrandModelsByBrandId($service->getCar()->brand_id);
        }

        return $this->render('form', ['model'   => $service->getCar(),
                                      'brands'  => $brands,
                                      'models'  => $models,
                                      'options' => $options,
        ]);
    }


    /** OLD method without service
     * @return string
     */
    /*public function actionAdd()
    {
        $car     = new Car();
        $brands  = $this->repo->getBrandsForDropDownList();
        $models  = [];
        $options = $this->repo->getOptionsForDropDownList();

        if (Yii::$app->request->isPost)
        {
            $car->load(Yii::$app->request->post());
            $car->photos = UploadedFile::getInstances($car, 'photos');

            if ($car->validate())
            {
                $car->save(false);
                $car->savePhotos();
                $car->saveOptions();

                $this->redirect(['cars/one', 'id' => $car->id]);
            }

            $models = $this->repo->getBrandModelsByBrandId($car->brand_id);
        }


        return $this->render('form', ['model'   => $car,
                                      'brands'  => $brands,
                                      'models'  => $models,
                                      'options' => $options,
        ]);
    }*/


}