<?php

use yii\db\Migration;

/**
 * Class m200214_105629_data
 */
class m200214_105629_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Brands
        $this->insert('{{%brands}}', ['id' => 1, 'name' => 'Audi']);
        $this->insert('{{%brands}}', ['id' => 2, 'name' => 'BMW']);


        // Brand Models
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => '100']);
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => '200']);
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => '80']);
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => 'A2']);
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => 'A5']);
        $this->insert('{{%brand_models}}', ['brand_id' => 1, 'name' => 'A6']);
        $this->insert('{{%brand_models}}', ['brand_id' => 2, 'name' => 'X3']);
        $this->insert('{{%brand_models}}', ['brand_id' => 2, 'name' => 'X4']);
        $this->insert('{{%brand_models}}', ['brand_id' => 2, 'name' => 'X5']);
        $this->insert('{{%brand_models}}', ['brand_id' => 2, 'name' => 'X6']);

        // Options
        $this->insert('{{%options}}', ['name' => 'ABS']);
        $this->insert('{{%options}}', ['name' => 'Break assist']);
        $this->insert('{{%options}}', ['name' => 'ESP']);
        $this->insert('{{%options}}', ['name' => 'Крепление детского сидения ISOFIX']);
        $this->insert('{{%options}}', ['name' => 'Парктроник']);
        $this->insert('{{%options}}', ['name' => 'Подушка безопасности водителя']);
        $this->insert('{{%options}}', ['name' => 'Подушка безопасности переднего пассажира']);
        $this->insert('{{%options}}', ['name' => 'Подушка безопасности боковые']);
        $this->insert('{{%options}}', ['name' => 'Сигнализация']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%options}}');
        $this->delete('{{%brand_models}}');
        $this->delete('{{%brands}}');
    }


}
