<?php

use yii\db\Migration;

/**
 * Class m200214_103223_auto
 */
class m200214_103223_cars extends Migration
{
    private $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brands}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
        ], $this->tableOptions);


        $this->createTable('{{%brand_models}}', [
            'id'         => $this->primaryKey(),
            'brand_id'   => $this->integer(),
            'name'       => $this->string(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_brand_models_brand_id', '{{%brand_models}}', 'brand_id', '{{%brands}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%cars}}', [
            'id'         => $this->primaryKey(),
            'brand_id'   => $this->integer(),
            'model_id'   => $this->integer(),
            'mileage'    => $this->integer(),
            'price'      => $this->integer(),
            'phone'      => $this->string(),
        ], $this->tableOptions);


        $this->addForeignKey('fk_cars_brand_id', '{{%cars}}', 'brand_id', '{{%brands}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_cars_model_id', '{{%cars}}', 'model_id', '{{%brand_models}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%car_photos}}', [
            'id'         => $this->primaryKey(),
            'car_id'     => $this->integer(),
            'name'       => $this->string(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_car_photos_car_id', '{{%car_photos}}', 'car_id', '{{%cars}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%options}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
        ], $this->tableOptions);


        $this->createTable('{{%car_options}}', [
            'car_id'    => $this->integer(),
            'option_id' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_car_options_car_id', '{{%car_options}}', 'car_id', '{{%cars}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_car_options_option_id', '{{%car_options}}', 'option_id', '{{%options}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_options}}');
        $this->dropTable('{{%options}}');
        $this->dropTable('{{%car_photos}}');
        $this->dropTable('{{%cars}}');
        $this->dropTable('{{%brand_models}}');
        $this->dropTable('{{%brands}}');
    }


}
