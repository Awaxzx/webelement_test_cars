$(document).ready(function () {

    $('body').on('click', '.delete', function (event) {

        event.stopPropagation();
        event.preventDefault();

        if (confirm('Вы уверенны?')) {
            $.ajax('/delete', {
                    method: 'post',
                    dataType: 'json',
                    data: 'id=' + $(this).attr('data-id'),
                    success: function (response) {

                        alert(response.message);
                        $('#cars').html(response.list);
                    }
                }
            );
        }

    });

    $('body').on('change', '#brand_id', function (event) {

        event.stopPropagation();
        event.preventDefault();

        $('#model_id option').remove();
        $('#model_id').append($("<option></option>").attr("value", 0).text('Загрузка моделей...'));
        $.ajax('/models/' + $("option:selected").val(), {
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#model_id option').remove();
                    $.each(response, function (key, value) {
                        $('#model_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            }
        );


    });

    $('body').on('click', '#hiddenCheckbox > label > input', function () {
        checkCheckboxes(this);
    });

    $('#hiddenCheckbox > label > input').each(function () {
        checkCheckboxes(this);
    });


    function checkCheckboxes(checkbox) {

        if ($(checkbox).prop('checked')) {
            $(checkbox).parent().addClass('checked');
        }
        else {
            $(checkbox).parent().removeClass('checked');
        }
    }


    $('body').on('click', '.gallery', function (event) {
        event.stopPropagation();
        event.preventDefault();

        $('#photo').attr('href', $(this).attr('data-original'));
        $('#photo > img').attr('src', $(this).attr('data-thumb2'));
    });
});