-- MySQL dump 10.13  Distrib 5.7.25, for Win64 (x86_64)
--
-- Host: localhost    Database: webelement_test_cars
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand_models`
--

DROP TABLE IF EXISTS `brand_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_brand_models_brand_id` (`brand_id`),
  CONSTRAINT `fk_brand_models_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_models`
--

LOCK TABLES `brand_models` WRITE;
/*!40000 ALTER TABLE `brand_models` DISABLE KEYS */;
INSERT INTO `brand_models` VALUES (1,1,'100'),(2,1,'200'),(3,1,'80'),(4,1,'A2'),(5,1,'A5'),(6,1,'A6'),(7,2,'X3'),(8,2,'X4'),(9,2,'X5'),(10,2,'X6');
/*!40000 ALTER TABLE `brand_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Audi'),(2,'BMW');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_options`
--

DROP TABLE IF EXISTS `car_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_options` (
  `car_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  KEY `fk_car_options_car_id` (`car_id`),
  KEY `fk_car_options_option_id` (`option_id`),
  CONSTRAINT `fk_car_options_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_car_options_option_id` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_options`
--

LOCK TABLES `car_options` WRITE;
/*!40000 ALTER TABLE `car_options` DISABLE KEYS */;
INSERT INTO `car_options` VALUES (1,1),(1,3),(1,4),(1,6),(1,7),(1,9),(3,1),(3,2),(3,4),(3,8),(3,6),(3,7),(3,9),(4,1),(4,2),(4,3),(4,4),(4,5),(4,8),(4,6),(4,7),(4,9),(5,2),(5,7),(6,1),(6,9),(7,4),(7,7);
/*!40000 ALTER TABLE `car_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_photos`
--

DROP TABLE IF EXISTS `car_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_car_photos_car_id` (`car_id`),
  CONSTRAINT `fk_car_photos_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_photos`
--

LOCK TABLES `car_photos` WRITE;
/*!40000 ALTER TABLE `car_photos` DISABLE KEYS */;
INSERT INTO `car_photos` VALUES (1,1,'5e47f47907804.jpg'),(2,1,'5e47f4791c6db.jpeg'),(3,1,'5e47f47932368.jpg'),(4,2,'5e47f4c0e96ff.jpg'),(5,2,'5e47f4c17ccfe.jpg'),(6,3,'5e47f4f5ac9c5.jpg'),(7,3,'5e47f4f5c60de.jpg'),(8,3,'5e47f4f5e7f3a.jpg'),(9,4,'5e47f55866548.jpeg'),(10,4,'5e47f558896c1.jpg'),(11,4,'5e47f5589ebcc.jpg'),(12,5,'5e47f57f6c39a.jpg'),(13,5,'5e47f57f9aadd.jpg'),(14,5,'5e47f57faaec8.jpg'),(15,6,'5e47f5ad86cf0.jpg'),(16,6,'5e47f5ad9e748.jpg'),(17,6,'5e47f5adb134f.jpg'),(18,7,'5e47f5d337c36.jpg'),(19,7,'5e47f5d353f09.jpg'),(20,7,'5e47f5d37d623.jpg');
/*!40000 ALTER TABLE `car_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cars_brand_id` (`brand_id`),
  KEY `fk_cars_model_id` (`model_id`),
  CONSTRAINT `fk_cars_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cars_model_id` FOREIGN KEY (`model_id`) REFERENCES `brand_models` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,1,1,123000,250000,'+7 (123) 456-78-90'),(2,1,4,100000,125500,'+7 (123) 456-78-99'),(3,1,5,NULL,1500000,'+7 (123) 564-78-90'),(4,2,9,2323232,34343434,'+7 (265) 656-56-56'),(5,1,2,100000,222222,'+7 (999) 999-99-99'),(6,2,8,323232323,23233232,'+7 (215) 566-56-56'),(7,1,6,232323,23232323,'+7 (489) 959-59-59');
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1581773805),('m200214_103223_cars',1581773806),('m200214_105629_data',1581773806);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'ABS'),(2,'Break assist'),(3,'ESP'),(4,'Крепление детского сидения ISOFIX'),(5,'Парктроник'),(6,'Подушка безопасности водителя'),(7,'Подушка безопасности переднего пассажира'),(8,'Подушка безопасности боковые'),(9,'Сигнализация');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-15 17:14:18
